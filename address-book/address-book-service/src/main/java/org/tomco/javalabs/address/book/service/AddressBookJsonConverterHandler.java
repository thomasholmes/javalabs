package org.tomco.javalabs.address.book.service;

import java.io.IOException;

import org.springframework.stereotype.Component;
import org.tomco.javalabs.address.book.domain.AddressBook;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Component
public class AddressBookJsonConverterHandler implements AddressBookJsonConverter {

    @Override
    public AddressBook convertJson(final String json) {

        AddressBook addressBook = null;
        final ObjectMapper objectMapper = new ObjectMapper();
        try {
            addressBook = objectMapper.readValue(json, AddressBook.class);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
        return addressBook;
    }

    @Override
    public String convertAddressBook(final AddressBook addressBook) {

        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
            return objectMapper.writeValueAsString(addressBook);
        } catch (final JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}