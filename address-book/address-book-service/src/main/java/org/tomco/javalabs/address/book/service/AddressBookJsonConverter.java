package org.tomco.javalabs.address.book.service;

import org.tomco.javalabs.address.book.domain.AddressBook;

public interface AddressBookJsonConverter {
    AddressBook convertJson(String json);

    String convertAddressBook(AddressBook addressBook);
}
