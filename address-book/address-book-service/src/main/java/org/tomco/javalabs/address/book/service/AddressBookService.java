package org.tomco.javalabs.address.book.service;

import org.tomco.javalabs.address.book.domain.AddressBook;
import org.tomco.javalabs.address.book.domain.Contact;

public interface AddressBookService {

    Contact createContact(AddressBook addressBook);
    
    void deleteContact(AddressBook addressBook, Contact contactToDelete);
}
