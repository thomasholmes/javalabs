package org.tomco.javalabs.address.book.service;

import java.util.ArrayList;
import java.util.List;

import org.tomco.javalabs.address.book.domain.AddressBook;
import org.tomco.javalabs.address.book.domain.Contact;

public class AddressBookServiceHandler implements AddressBookService {

    @Override
    public Contact createContact(final AddressBook addressBook) {

        final List<Contact> contactList = addressBook.getContactList();
        final List<Contact> updatedContactList = new ArrayList<>();

        final Contact newContact = new Contact();
        updatedContactList.addAll(contactList);
        updatedContactList.add(newContact);

        addressBook.setContactList(updatedContactList);
        return newContact;
    }

    @Override
    public void deleteContact(final AddressBook addressBook, final Contact contactToDelete) {

        final List<Contact> updatedContactList = new ArrayList<>();
        for (final Contact contact : addressBook.getContactList()) {
            if (contact.equals(contact) == false) {
                updatedContactList.add(contact);
            }
        }
        addressBook.setContactList(updatedContactList);
    }

}
