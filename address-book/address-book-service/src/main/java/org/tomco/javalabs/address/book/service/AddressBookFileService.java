package org.tomco.javalabs.address.book.service;

import java.io.IOException;

import org.tomco.javalabs.address.book.domain.AddressBook;

public interface AddressBookFileService {

    void SaveAddressBookToFile(AddressBook addressBook, String filePath) throws IOException;

    AddressBook LoadAddressBookFromFile(String filePath) throws IOException;
}
