package org.tomco.javalabs.address.book.service;

import java.io.File;
import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;
import org.tomco.javalabs.address.book.domain.AddressBook;

@Component
public class AddressBookFileServiceHandler implements AddressBookFileService {

    private AddressBookJsonConverter addressBookJsonConverter;

    @Override
    public void SaveAddressBookToFile(final AddressBook addressBook, final String filePath) throws IOException {

        final String addressBookJson = addressBookJsonConverter.convertAddressBook(addressBook);

        final File addressBookFile = new File(filePath);
        FileUtils.writeStringToFile(addressBookFile, addressBookJson);
    }

    @Override
    public AddressBook LoadAddressBookFromFile(final String filePath) throws IOException {

        final File addressBookFile = new File(filePath);
        final String addressBookJson = FileUtils.readFileToString(addressBookFile);

        final AddressBook addressBook = addressBookJsonConverter.convertJson(addressBookJson);
        return addressBook;
    }

    @Resource
    public void setAddressBookJsonConverter(final AddressBookJsonConverter addressBookJsonConverter) {
        this.addressBookJsonConverter = addressBookJsonConverter;
    }

}
