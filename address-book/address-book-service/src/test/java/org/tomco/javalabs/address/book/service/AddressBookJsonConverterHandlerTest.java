/**
 * 
 */
package org.tomco.javalabs.address.book.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tomco.javalabs.address.book.domain.AddressBook;
import org.tomco.javalabs.address.book.domain.Contact;

/**
 * Name converter handler test.
 */
public class AddressBookJsonConverterHandlerTest {

    /** Class under test. */
    private AddressBookJsonConverterHandler nameConverterHandler;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        nameConverterHandler = new AddressBookJsonConverterHandler();
    }

    @Test
    public void checkConvertJson() throws IOException {

        // Setup
        final String json = FileUtils.readFileToString(new File("src/test/resources/addressBook.json"));

        // Expectations
        final Contact contact1 = new Contact();
        contact1.setName("Thomas Holmes");
        contact1.setTelephone("012345");
        contact1.setEmailAddress("thomasholmes@outlook.com");

        final Contact contact2 = new Contact();
        contact2.setName("Tom Holm");
        contact2.setTelephone("67890");
        contact2.setEmailAddress("tomholm@outlook.com");

        final List<Contact> contactList = new ArrayList<>();
        contactList.add(contact1);
        contactList.add(contact2);

        final AddressBook expectedAddressBook = new AddressBook();
        expectedAddressBook.setContactList(contactList);

        // Test
        final AddressBook result = nameConverterHandler.convertJson(json);

        // Assert
        Assert.assertEquals(expectedAddressBook, result);
    }

    @Test
    public void checkConvertContacts() throws IOException {

        // Setup
        final Contact contact1 = new Contact();
        contact1.setName("Thomas Holmes");
        contact1.setTelephone("012345");
        contact1.setEmailAddress("thomasholmes@outlook.com");

        final Contact contact2 = new Contact();
        contact2.setName("Tom Holm");
        contact2.setTelephone("67890");
        contact2.setEmailAddress("tomholm@outlook.com");

        final List<Contact> contactList = new ArrayList<>();
        contactList.add(contact1);
        contactList.add(contact2);

        final AddressBook addressBook = new AddressBook();
        addressBook.setContactList(contactList);

        // Expectations
        final String expectedJson = FileUtils.readFileToString(new File("src/test/resources/addressBook.json"));

        // Test
        final String json = nameConverterHandler.convertAddressBook(addressBook);

        // Assert
        Assert.assertEquals(expectedJson, json);
    }
}
