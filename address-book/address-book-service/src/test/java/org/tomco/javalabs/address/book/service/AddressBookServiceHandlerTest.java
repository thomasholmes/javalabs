/**
 * 
 */
package org.tomco.javalabs.address.book.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tomco.javalabs.address.book.domain.AddressBook;
import org.tomco.javalabs.address.book.domain.Contact;


/**
 * Address book service handler test.
 */
public class AddressBookServiceHandlerTest {

    /** Class under test. */
    private AddressBookServiceHandler addressBookServiceHandler;
    
    /**
     * Setup.
     */
    @Before
    public void setup() {
        addressBookServiceHandler = new AddressBookServiceHandler();
    }
    
    @Test
    public void checkCreateContact() {
        
        // Setup
        final AddressBook addressBook = new AddressBook();
        addressBook.setContactList(new ArrayList<Contact>());
        
        // Test
        final Contact contact = addressBookServiceHandler.createContact(addressBook);
        
        // Assert
        Assert.assertEquals(1, addressBook.getContactList().size());
        Assert.assertEquals(addressBook.getContactList().get(0), contact);
    }
    
    @Test
    public void checkDeleteContact() {
        
        // Setup
        final List<Contact> contactList = new ArrayList<>();
        final Contact contact = new Contact();
        contactList.add(contact);
        
        final AddressBook addressBook = new AddressBook();
        addressBook.setContactList(contactList);
        
        // Test
        addressBookServiceHandler.deleteContact(addressBook, contact);
        
        // Assert
        Assert.assertEquals(0, addressBook.getContactList().size());
    }
}
