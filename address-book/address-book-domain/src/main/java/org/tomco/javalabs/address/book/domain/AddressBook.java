package org.tomco.javalabs.address.book.domain;

import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class AddressBook {

    private List<Contact> contactList;

    public List<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(final List<Contact> contactList) {
        this.contactList = contactList;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(contactList).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append(contactList).toString();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        final AddressBook otherAddressBook = (AddressBook) obj;
        return new EqualsBuilder().append(contactList, otherAddressBook.contactList).isEquals();
    }

}
