package org.tomco.javalabs.address.book.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Contact {

    private String name;

    private String telephone;

    private String emailAddress;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(final String telephone) {
        this.telephone = telephone;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(final String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(name).append(telephone).append(emailAddress).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append(name).append(telephone).append(emailAddress).toString();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        final Contact otherContact = (Contact) obj;
        return new EqualsBuilder().append(name, otherContact.name).append(telephone, otherContact.telephone)
                .append(emailAddress, otherContact.emailAddress).isEquals();
    }

}