package org.tomco.javalabs.address.book;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AddressBookApplication extends Application {

    public static void main(final String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage stage) throws Exception {

        final ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");

        final SpringFxmlLoader loader = new SpringFxmlLoader(context);

        final Parent root = (Parent) loader.load("AddressBook.fxml", AddressBookController.class);
        final Scene scene = new Scene(root, 800, 500);

        stage.setScene(scene);
        stage.setTitle("Address Book by TomCo");
        stage.show();

    }

}
