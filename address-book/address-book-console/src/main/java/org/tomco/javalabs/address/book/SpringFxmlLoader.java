package org.tomco.javalabs.address.book;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.context.ApplicationContext;

import javafx.fxml.FXMLLoader;

public class SpringFxmlLoader {
    private final ApplicationContext context;

    public SpringFxmlLoader(final ApplicationContext context) {
        this.context = context;
    }

    public Object load(final String url, final Class<?> controllerClass) throws IOException {
        try (InputStream fxmlStream = controllerClass.getResourceAsStream(url)) {
            final Object instance = context.getBean(controllerClass);
            final FXMLLoader loader = new FXMLLoader();
            loader.getNamespace().put("controller", instance);
            return loader.load(fxmlStream);
        }
    }
}
