package org.tomco.javalabs.starwarsnamegenerator.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.tomco.javalabs.starwarsnamegenerator.domain.Name;
import org.tomco.javalabs.starwarsnamegenerator.domain.StarWarsName;

@Component
public class NameConverterHandler implements NameConverter {

    @Override
    public StarWarsName convertName(final Name name) {

        final StarWarsName starWarsName = new StarWarsName();

        final String firstName = StringUtils.join(
                StringUtils.defaultIfBlank(name.getFirstName(), StringUtils.EMPTY).subSequence(0, 3),
                StringUtils.defaultIfBlank(name.getSurName(), StringUtils.EMPTY).subSequence(0, 2));
        starWarsName.setFirstName(firstName);

        final String surname = StringUtils.join(
                StringUtils.defaultIfBlank(name.getMaidenName(), StringUtils.EMPTY).subSequence(0, 2),
                StringUtils.defaultIfBlank(name.getBirthPlace(), StringUtils.EMPTY).subSequence(0, 3));
        starWarsName.setSurname(surname);

        return starWarsName;
    }
}