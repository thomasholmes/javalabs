package org.tomco.javalabs.starwarsnamegenerator.service;

import org.tomco.javalabs.starwarsnamegenerator.domain.Name;
import org.tomco.javalabs.starwarsnamegenerator.domain.StarWarsName;

public interface NameConverter {
    StarWarsName convertName(Name name);
}
