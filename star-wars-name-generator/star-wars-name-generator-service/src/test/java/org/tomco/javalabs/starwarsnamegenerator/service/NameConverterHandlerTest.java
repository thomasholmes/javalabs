/**
 * 
 */
package org.tomco.javalabs.starwarsnamegenerator.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tomco.javalabs.starwarsnamegenerator.domain.Name;
import org.tomco.javalabs.starwarsnamegenerator.domain.StarWarsName;


/**
 * Name converter handler test.
 */
public class NameConverterHandlerTest {

    /** Class under test. */
    private NameConverterHandler nameConverterHandler;
    
    /**
     * Setup.
     */
    @Before
    public void setup() {
        nameConverterHandler = new NameConverterHandler();
    }
    
    @Test
    public void checkConvertName() {
        
        // Setup
        final Name name = new Name();
        name.setFirstName("Thomas");
        name.setSurName("Holmes");
        name.setBirthPlace("England");
        name.setMaidenName("Mary");
        
        // Test
        final StarWarsName starWarsName = nameConverterHandler.convertName(name);
        
        // Assert
        Assert.assertEquals("ThoHo", starWarsName.getFirstName());
        Assert.assertEquals("MaEng", starWarsName.getSurname());
    }
}
