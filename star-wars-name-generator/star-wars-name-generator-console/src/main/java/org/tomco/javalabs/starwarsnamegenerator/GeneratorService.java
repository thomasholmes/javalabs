package org.tomco.javalabs.starwarsnamegenerator;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tomco.javalabs.starwarsnamegenerator.domain.Name;
import org.tomco.javalabs.starwarsnamegenerator.domain.StarWarsName;
import org.tomco.javalabs.starwarsnamegenerator.service.NameConverter;

@Component
public class GeneratorService {

    private NameConverter nameConverter;

    public void start() {
        try (Scanner input = new Scanner(System.in)) {

            System.out.println("Please enter your first name:");
            final String firstName = input.next();

            System.out.println("Please enter your surname:");
            final String surname = input.next();

            System.out.println("Please enter your mother's maiden name:");
            final String maidenName = input.next();

            System.out.println("Please enter your place of birth:");
            final String birthPlace = input.next();

            final Name name = new Name();
            name.setFirstName(firstName);
            name.setSurName(surname);
            name.setMaidenName(maidenName);
            name.setBirthPlace(birthPlace);

            final StarWarsName starWarsName = nameConverter.convertName(name);

            System.out.println(String.format("Your Star Wars name is %s %s.", starWarsName.getFirstName(),
                    starWarsName.getSurname()));

        }
    }

    @Autowired
    public void setNameConverter(final NameConverter nameConverter) {
        this.nameConverter = nameConverter;
    }

}
