package org.tomco.javalabs.starwarsnamegenerator;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Console {

    public static void main(final String[] args) {

        try (final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml")) {

            final GeneratorService generatorService = context.getBean(GeneratorService.class);
            generatorService.start();
        }

    }

}