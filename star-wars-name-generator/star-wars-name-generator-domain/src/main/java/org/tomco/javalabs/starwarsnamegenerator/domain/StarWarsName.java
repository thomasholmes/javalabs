package org.tomco.javalabs.starwarsnamegenerator.domain;

public class StarWarsName {

    private String firstName;

    private String surname;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(final String surname) {
        this.surname = surname;
    }

}